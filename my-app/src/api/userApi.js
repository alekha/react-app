import axios from 'axios';
const base_url =  process.env.REACT_APP_API_URI;

const createUser = async (data) => {
    const config = {
        'Content-Type': 'application/json'
    };
    let response = await axios.post(base_url + '/user/signup', data, { headers: config })
        .then(function (response) {
            return response;
        })
        .catch(function (error) {
            return error;
        });
    return response;
}

const loginUser = async (data) => {
    const config = {
        'Content-Type': 'application/json'
    };
    let response = await axios.post(base_url + '/user/login', data, { headers: config })
        .then(function (response) {
            return response;
        })
        .catch(function (error) {
            return error;
        });
    return response;
}

const singleUser = async (id) => {
    const config = {
        'Content-Type': 'application/json'
    };
    const response = await axios.get(base_url + '/user/' + id,{ headers: config })
        .then(function (response) {
            return response;
        })
        .catch(function (error) {
            return error;
        });
    return response;
}

const updateUser = async (id,data) => {
    const config = {
        'Content-Type': 'application/json'
    };
    let response = await axios.put(base_url + `/user/${id}`, data, { headers: config })
        .then(function (response) {
            return response;
        })
        .catch(function (error) {
            return error;
        });
    return response;
}
const deleteSingleUser = async (id) => {
    const config = {
        'Content-Type': 'application/json'
    };
    let response = await axios.delete(base_url + `/user/${id}`, { headers: config })
        .then(function (response) {
            return response;
        })
        .catch(function (error) {
            return error;
        });
    return response;
}
const changePassword = async (data) => {
    const config = {
        'Content-Type': 'application/json'
    };
    let response = await axios.post(base_url + '/user/change-password', data, { headers: config })
        .then(function (response) {
            return response;
        })
        .catch(function (error) {
            return error;
        });
    return response;
}

export { createUser,  loginUser, singleUser,updateUser,deleteSingleUser,changePassword};