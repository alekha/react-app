import { useState } from "react";
import { toast } from "react-toastify";
import { createUser } from "../../api/userApi";
import 'react-toastify/dist/ReactToastify.css';

export const UseSignUpHooks = () => {
    const [userDetail, setUserDetail] = useState({
        firstName: '',
        lastName: '',
        email: '',
        phone: '',
        status: '',
        dob: '',
        password: ''
    })
    const [createPassword, setCreatePassword] = useState('')
    const [showNoValidEmail, setShowNoValidEmail] = useState(false);
    const [showNoValidName, setShowNoValidName] = useState(false);
    const [showNoValidLastName, setShowNoValidLastName] = useState(false);
    const [showNoValidPhone, setShowNoValidPhone] = useState(false);
    const [showNoValidPassword, setShowNoValidPassword] = useState(false);
    const [showPassword, setShowPassword] = useState(false);


    const sumbitHandler = async (e) => {
        e.preventDefault();
        if (createPassword !== userDetail.password) {
            toast.warning("Password and confirm password do not matching", { autoClose: 1000 })
        }
        else {
            let res = await createUser(userDetail);
            if (res.data?.status) {
                toast.success(res.data?.msg, { autoClose: 1000 })
                window.location.assign("/")
            }
            else if (res?.response?.data?.error?.message) {
                toast.warning(res?.response?.data?.error?.message, { autoClose: 1000 })
            }
            else {
                toast.warning(res?.response?.data?.msg ?? res.data?.response?.error?.msg, { autoClose: 1000 })
            }
        }
    };
    return {
        userDetail, setUserDetail,
        createPassword, setCreatePassword,
        showNoValidEmail, setShowNoValidEmail,
        showNoValidName, setShowNoValidName,
        showNoValidLastName, setShowNoValidLastName,
        showNoValidPhone, setShowNoValidPhone,
        showNoValidPassword, setShowNoValidPassword,
        showPassword, setShowPassword,
        sumbitHandler
    }
}