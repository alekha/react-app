import React, { useState } from 'react'
import { Col, Container, Row } from 'react-bootstrap';
import {
    validateName,
    validateEmail,
    validatePassword,
    validatePhone
} from "../../utils/form-validators"
import { BsEye, BsEyeSlash } from "react-icons/bs"
import { UseSignUpHooks } from './hooks';
import { toast } from 'react-toastify';


const SignUp = () => {
    const {
        userDetail, setUserDetail,
        createPassword, setCreatePassword,
        showNoValidEmail, setShowNoValidEmail,
        showNoValidName, setShowNoValidName,
        showNoValidLastName, setShowNoValidLastName,
        showNoValidPhone, setShowNoValidPhone,
        showNoValidPassword, setShowNoValidPassword,
        showPassword, setShowPassword,
        sumbitHandler
    }= UseSignUpHooks()

    return (
        <Container fluid className="py-5  d-flex align-item-center justify-content-center">
            <div className="w-50 my-4 p-4 position-relative signUpDiv">
                <form onSubmit={sumbitHandler}>
                    <h3 className="text-center fw-bold mb-3">Sign Up</h3>
                    <Row>
                        <Col md="6">

                            <div className="mb-3">
                                <label>First Name</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    placeholder="Enter First Name"
                                    name="firstName"
                                    onChange={(e) => setUserDetail({ ...userDetail, firstName: e.target.value })}
                                    onKeyUp={(e) => setShowNoValidName(validateName(e))}
                                />
                                <p className="text-danger">
                                    {showNoValidName ? "Invalid first name" : ""}
                                </p>
                            </div>
                        </Col>
                        <Col md="6">

                            <div className="mb-3">
                                <label>Last Name</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    placeholder="Enter Last Name"
                                    name="lastName"
                                    onChange={(e) => setUserDetail({ ...userDetail, lastName: e.target.value })}
                                    onKeyUp={(e) => setShowNoValidLastName(validateName(e))}
                                />
                                <p className="text-danger">
                                    {showNoValidLastName ? "Invalid first name" : ""}
                                </p>
                            </div>
                        </Col>
                    </Row>
                    <Row>
                        <Col md="6">

                            <div className="mb-3">
                                <label>Email</label>
                                <input
                                    type="email"
                                    className="form-control"
                                    placeholder="Enter Email"
                                    name="email"
                                    onChange={(e) => setUserDetail({ ...userDetail, email: e.target.value })}
                                    onKeyUp={(e) => setShowNoValidEmail(validateEmail(e))}
                                />
                                <p className="text-danger">
                                    {showNoValidEmail ? "Invalid mail address" : ""}
                                </p>
                            </div>
                        </Col>
                        <Col md="6">
                            <div className="mb-3">
                                <label>Phone Number</label>
                                <input
                                    type="number"
                                    className="form-control"
                                    placeholder="Enter Phone Number"
                                    name="phone"
                                    onChange={(e) => setUserDetail({ ...userDetail, phone: e.target.value })}
                                    onKeyUp={(e) => setShowNoValidPhone(validatePhone(e))}
                                />
                                <p className="text-danger">
                                    {showNoValidPhone ? "Invalid phone number" : ""}
                                </p>
                            </div>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <div className="mb-3 position-relative">
                                <label> Create Password</label>
                                <input
                                    type={!showPassword ? "password" : "text"}
                                    className="form-control"
                                    placeholder="Enter Create Password"
                                    name="password"
                                    onChange={(e) => setUserDetail({ ...userDetail, password: e.target.value })}
                                    onKeyUp={(e) =>
                                        setShowNoValidPassword(validatePassword(e))
                                    }
                                />
                                <div
                                    className="position-absolute top-0 end-0 pe-2 showPassWord"
                                    onClick={() => setShowPassword(!showPassword)}
                                >
                                    {showPassword ? <BsEye /> : <BsEyeSlash />}
                                </div>
                                <p className="text-danger">
                                    {showNoValidPassword
                                        ? "Password contains 6 or more characters with a mix of lowercase & uppercase letters, numbers & symbols"
                                        : ""}
                                </p>
                            </div>
                        </Col>
                        <Col>
                            <div className="mb-3">
                                <label>Confirm Password</label>
                                <input
                                    type="password"
                                    className="form-control"
                                    placeholder="Enter Confirm Password"
                                    name="createPassword"
                                    onChange={(e) => setCreatePassword(e.target.value)}
                                    onPaste={(e) => {
                                        toast.warning("Cann't paste", { autoClose: 1000 })
                                    }}
                                />
                            </div>
                        </Col>
                    </Row>
                    <Row>
                        <Col md="6">

                            <div className="mb-3">
                                <label>Date Of Birth</label>
                                <input
                                    type="date"
                                    className="form-control"
                                    placeholder="Enter Date Of Birth"
                                    name="dob"
                                    onChange={(e) => setUserDetail({ ...userDetail, dob: e.target.value })}
                                />
                            </div>
                        </Col>
                        <Col md="6">
                            <div className="mb-3">
                                <label>Status</label>
                                <div>
                                    <select value={userDetail?.status} onChange={(e) => setUserDetail({ ...userDetail, status: e.target.value })}
                                        className="selectDiv w-100">
                                        <option value="">Select an option</option>
                                        <option value="PENDING">Pending</option>
                                        <option value="ACTIVE">Active</option>
                                        <option value="DE-ACTIVE">De-Active</option>
                                    </select>
                                </div>
                            </div>
                        </Col>
                    </Row>
                    <Row>
                        <Col md="12" className="text-center">
                            <div>
                                <button type="submit" className="btn btn-primary">
                                    Sign up
                                </button>
                            </div>
                        </Col>
                    </Row>
                    <p className="forgot-password text-right">
                        Have Already An Account ? <a href="/">LogIn Here</a>
                    </p>
                </form >
            </div >
        </Container>
    )
}
export default SignUp