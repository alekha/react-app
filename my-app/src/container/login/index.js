import React, { useEffect, useState } from 'react';
import { loginUser } from '../../api/userApi';
import { toast } from 'react-toastify';
import ProfilePage from '../profile';
import { Container } from 'react-bootstrap';


const LogIn = () => {
    const [password, setPassword] = useState('')
    const [email, setEmail] = useState('')
    const sumbitHandler = async (e) => {
        e.preventDefault();
        let res = await loginUser({ email, password });
        if (res.data?.status) {
            toast.success(res.data?.msg, { autoClose: 1000 })
            localStorage.setItem('LOGGED_USER', JSON.stringify(res.data?.user));
            window.location.assign(`/profile/${res.data?.user?.userId}`)
        } else {
            toast.warning(res.data?.msg, { autoClose: 1000 })
        }
    }

    const loggedUser = localStorage.getItem('LOGGED_USER');
    useEffect(() => {
        if (loggedUser) {
            const user = JSON.parse(loggedUser)
            window.location.assign(`/profile/${user?.userId}`)
        }
    })

    return (
        <Container fluid className="py-5  d-flex align-item-center justify-content-center">
            {
                loggedUser ?
                    <ProfilePage />
                    :
                    <div className="p-4 loginDiv">
                        <form onSubmit={sumbitHandler}>
                            <h3 className="text-center fw-bold mb-3">Sign In</h3>
                            <div className="mb-3">
                                <label>Email address</label>
                                <input
                                    type="email"
                                    className="form-control mt-2"
                                    placeholder="Enter email"
                                    name="email"
                                    onChange={(e) => setEmail(e.target.value)}
                                />
                            </div>
                            <div className="mb-3">
                                <label>Password</label>
                                <input
                                    type="password"
                                    className="form-control mt-2"
                                    placeholder="Enter password"
                                    name="password"
                                    onChange={(e) => setPassword(e.target.value)}
                                />
                            </div>
                            <div className="d-grid">
                                <button type="submit" className="btn btn-primary">
                                    Login
                                </button>
                            </div>
                            <p className="forgot-password text-right mt-4">
                                Not Registered ? <a href="/signup">Click Here</a>
                            </p>
                        </form>
                    </div>
            }
        </Container >
    )
}
export default LogIn