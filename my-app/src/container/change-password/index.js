import React, { useState } from 'react';
import { UseChangePassword } from './hooks';
import {validatePassword } from '../../utils/form-validators';
import { Col, Row } from 'react-bootstrap';
const MyForm = () => {
    const {
        singleUser, setSingleUser,
        showNoValidPassword,
        handleSubmit,
        setShowNoValidPassword
    } = UseChangePassword()


    return (
        <form onSubmit={handleSubmit}>
            <Row>
                <Col md="12">
                    <div className="text-center fw-bold mb-3">Change Password</div>
                </Col>

            </Row>
            <Row>
                <Col md="6">
                    <div className="mb-3">
                        <label>Email</label>
                        <input
                            type="email"
                            className="form-control"
                            placeholder="Email"
                            name="email"
                            value={singleUser?.email}
                            onChange={(e) => setSingleUser({ ...singleUser, email: e.target.value })}
                           
                        />

                    </div>
                </Col>
                <Col>
                    <div className="mb-3 position-relative">
                        <label>Password</label>
                        <input
                            type="password"
                            className="form-control"
                            placeholder="Enter Current Password"
                            name="password"
                            value={singleUser?.password}
                            onChange={(e) => setSingleUser({ ...singleUser, password: e.target.value })}

                        />
                    </div>
                </Col>
            </Row>
            <Row>

                <Col>
                    <div className="mb-3">
                        <label>New Password</label>
                        <input
                            type="password"
                            className="form-control"
                            placeholder="Enter New Password"
                            name="newPassword"
                            value={singleUser?.newPassword}
                            onChange={(e) => setSingleUser({ ...singleUser, newPassword: e.target.value })}
                            onKeyUp={(e) =>
                                setShowNoValidPassword(validatePassword(e))
                            }
                        />
                        <p className="text-danger">
                            {showNoValidPassword
                                ? "Password contains 6 or more characters with a mix of lowercase & uppercase letters, numbers & symbols"
                                : ""}
                        </p>
                    </div>
                </Col>
            </Row>

            <Row>
                <Col md="12" className="text-center">
                    <div>
                        <button type="submit" className="btn btn-primary">
                            Save
                        </button>
                    </div>
                </Col>
            </Row>
        </form>
    );
};

export default MyForm;
