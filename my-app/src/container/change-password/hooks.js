import { useState } from "react";
import { changePassword } from "../../api/userApi";
import { toast } from "react-toastify";

export const UseChangePassword = () => {
    const [singleUser, setSingleUser] = useState({
        email: '',
        password: '',
        newPassword: ''
    })
    const [showNoValidPassword, setShowNoValidPassword] = useState(false);

    const handleSubmit = async (e) => {
        e.preventDefault();
        let res = await changePassword(singleUser);
        if (res?.status) {
            toast.success(res?.msg, { autoClose: 1000 })
            localStorage.removeItem('LOGGED_USER')
            window.location.assign("/")
        }
        toast.warning(res?.response?.data?.msg, { autoClose: 1000 })
    };
    return {
        singleUser, setSingleUser,
        showNoValidPassword, setShowNoValidPassword,
        handleSubmit,

    }

}