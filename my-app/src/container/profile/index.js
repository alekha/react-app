import React, { useEffect, useState } from 'react'
import { Col, Container, Row } from 'react-bootstrap';
import {
    validateName,
    validateEmail,
    validatePhone
} from "../../utils/form-validators"
import MyForm from '../change-password';
import { useProfileHook } from './hooks';

const ProfilePage = () => {
    const {
        userDetail,
        showNoValidEmail,
        showNoValidName,
        showNoValidLastName,
        showNoValidPhone,
        isBoxVisible,
        changePasswordForm,
        setShowNoValidLastName,
        setShowNoValidEmail,
        setShowNoValidName,
        setShowNoValidPhone,
        setIsBoxVisible,
        setUserDetail,
        sumbitHandler,
        userLogout,
        handleMouseEnter,
        deleteUser,
        changePassword
    } = useProfileHook();

    return (
        <Container fluid className="py-5  d-flex align-item-center justify-content-center">

            <div className="w-50 p-4 profileDiv my-4 position-relative" onClick={() => setIsBoxVisible(false)}>
                {changePasswordForm ?
                    <MyForm /> :
                    <form onSubmit={sumbitHandler}>
                        <Row>
                            <Col md="7">
                                <div className="text-end fw-bold mb-3">Update Detail</div>
                            </Col>
                            <Col>
                                <div className="text-end mb-3" >
                                    <span className="text" onMouseEnter={handleMouseEnter}
                                    >:</span>
                                    {isBoxVisible && (
                                        <div className="box">
                                            <ul>
                                                <li onClick={() => { userLogout(); setIsBoxVisible(false) }} role="button">Logout</li>
                                                <li onClick={() => deleteUser()} role="button">Delete Profile</li>
                                                <li onClick={() => changePassword()} role="button">Change Password</li>
                                            </ul>
                                        </div>
                                    )}
                                </div>
                            </Col>
                        </Row>
                        <Row>
                            <Col md="6">
                                <div className="mb-3">
                                    <label>First Name</label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        placeholder="Enter First Name"
                                        name="firstName"
                                        value={userDetail?.firstName}
                                        onChange={(e) => setUserDetail({ ...userDetail, firstName: e.target.value })}
                                        onKeyUp={(e) => setShowNoValidName(validateName(e))}
                                    />
                                    <p className="text-danger">
                                        {showNoValidName ? "Invalid first name" : ""}
                                    </p>
                                </div>
                            </Col>
                            <Col md="6">

                                <div className="mb-3">
                                    <label>Last Name</label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        placeholder="Enter Last Name"
                                        name="lastName"
                                        value={userDetail?.lastName}
                                        onChange={(e) => setUserDetail({ ...userDetail, lastName: e.target.value })}
                                        onKeyUp={(e) => setShowNoValidLastName(validateName(e))}
                                    />
                                    <p className="text-danger">
                                        {showNoValidLastName ? "Invalid first name" : ""}
                                    </p>
                                </div>
                            </Col>
                        </Row>
                        <Row>
                            <Col md="6">

                                <div className="mb-3">
                                    <label>Email</label>
                                    <input
                                        type="email"
                                        className="form-control"
                                        placeholder="Enter Email"
                                        name="email"
                                        value={userDetail?.email}
                                        onChange={(e) => setUserDetail({ ...userDetail, email: e.target.value })}
                                        onKeyUp={(e) => setShowNoValidEmail(validateEmail(e))}
                                    />
                                    <p className="text-danger">
                                        {showNoValidEmail ? "Invalid mail address" : ""}
                                    </p>
                                </div>
                            </Col>
                            <Col md="6">
                                <div className="mb-3">
                                    <label>Phone</label>
                                    <input
                                        type="number"
                                        className="form-control"
                                        placeholder="Enter Phone Number"
                                        name="phone"
                                        value={userDetail?.phone}
                                        onChange={(e) => setUserDetail({ ...userDetail, phone: e.target.value })}
                                        onKeyUp={(e) => setShowNoValidPhone(validatePhone(e))}
                                    />
                                    <p className="text-danger">
                                        {showNoValidPhone ? "Invalid phone number" : ""}
                                    </p>
                                </div>
                            </Col>
                        </Row>

                        <Row>
                            <Col md="6">

                                <div className="mb-3">
                                    <label>Date Of Birth</label>
                                    <input
                                        type="date"
                                        id="dateInput"
                                        className="form-control"
                                        placeholder="Enter Date Of Birth"
                                        name="dob"
                                        value={userDetail?.dob}
                                        onChange={(e) => setUserDetail({ ...userDetail, dob: e.target.value })}
                                    />
                                </div>
                            </Col>
                            <Col md="6">
                                <div className="mb-3">
                                    <label>Status</label>
                                    <div>
                                        <select value={userDetail?.status} onChange={(e) => setUserDetail({ ...userDetail, status: e.target.value })}
                                            className="selectDiv w-100">
                                            <option value="">Select an option</option>
                                            <option value="PENDING">Pending</option>
                                            <option value="ACTIVE">Active</option>
                                            <option value="DE-ACTIVE">De-Active</option>
                                        </select>
                                    </div>
                                </div>
                            </Col>
                        </Row>
                        <Row className="mb-0">
                            <Col md="12" className="text-center">
                                <div>
                                    <button type="submit" className="btn btn-primary">
                                        Update
                                    </button>
                                </div>
                            </Col>
                        </Row>
                    </form >
                }
            </div >
        </Container>
    )
}
export default ProfilePage