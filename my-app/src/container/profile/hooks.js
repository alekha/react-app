import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { deleteSingleUser, singleUser, updateUser } from '../../api/userApi';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


export const useProfileHook = ()=>{
    const { userId } = useParams();
    const [userDetail, setUserDetail] = useState({
        firstName: '',
        lastName: '',
        email: '',
        phone: '',
        status: '',
        dob: ''
    })

    const [showNoValidEmail, setShowNoValidEmail] = useState(false);
    const [showNoValidName, setShowNoValidName] = useState(false);
    const [showNoValidLastName, setShowNoValidLastName] = useState(false);
    const [showNoValidPhone, setShowNoValidPhone] = useState(false);
    const loggedUser = localStorage.getItem('LOGGED_USER');
    const [isBoxVisible, setIsBoxVisible] = useState(false);
    const [changePasswordForm, setChangePasswordForm] = useState(false)

    useEffect(() => {
        getSingleDetail(userId)
    }, [userId])

    const sumbitHandler = async (e) => {
        e.preventDefault();
        let res = await updateUser(userId, userDetail);
        if (res.data?.status) {
            toast.success(res.data?.msg, { autoClose: 1000 })
        } else {
            toast.warning(res?.data?.msg ? res.data?.msg : res.data?.error, { autoClose: 1000 })
        }
    };

    const userLogout = () => {
        if (loggedUser) {
            localStorage.removeItem('LOGGED_USER')
            toast.success("Logged out!", { autoClose: 1000 })
            window.location.assign('/')
        }
    }

    const getSingleDetail = async (userId) => {
        const res = await singleUser(userId);
        if (res?.data?.status) {
            setUserDetail(res?.data?.user)
        }
    }
 

    const handleMouseEnter = () => {
        setIsBoxVisible(true);
    };

    const changePassword = () => {
        setChangePasswordForm(true)
    };

    const deleteUser = async () => {
        setIsBoxVisible(false);
        const res = await deleteSingleUser(userId)
        if (res?.data?.status) {
            toast.success(res?.data?.msg, { autoClose: 1000 })
            localStorage.removeItem('LOGGED_USER')
        }
    };
    return{
        userDetail,
        setUserDetail,
        showNoValidEmail, setShowNoValidEmail,
        showNoValidName, setShowNoValidName,
        showNoValidLastName, setShowNoValidLastName,
        showNoValidPhone, setShowNoValidPhone,
        loggedUser,
        isBoxVisible, setIsBoxVisible,
        changePasswordForm, setChangePasswordForm,
        sumbitHandler,
        userLogout,
        getSingleDetail,
        handleMouseEnter,
        deleteUser,
        changePassword
    }
}