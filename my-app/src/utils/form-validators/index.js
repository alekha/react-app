const regex={
    name: /^(?=.{0,30}$)(?![_.])(?!.*[_.]{2})[a-zA-Z._]+(?<![_.])$/g,
    email: /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g,
    phone: /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/g,
    password: /^(?=.*\d)(?=.*[a-z])(?=.*[!@#$%^&*])(?=.*[A-Z]).{6,}$/g
}

export const validateName = (e) => {
    if (e.target.value?.length) {
        if (e.target?.value && e.target.value.match(regex.name)) {
            return false
        } else {
            return true
        }
    }

}
export const validatePhone = (e) => {
    if (e.target.value?.length) {
        if (e.target?.value && e.target.value.match(regex.phone)) {
            return false
        } else {
            return true
        }
    }

}
export const validatePassword = (e) => {
    if (e.target.value?.length) {
        if (e.target?.value && e.target.value.match(regex.password)) {
            return false
        } else {
            return true
        }
    }

}

export const validateEmail = (e) => {
    if (e.target.value?.length) {
        if (e.target?.value && e.target.value.match(regex.email)) {
            return false
            // setForm({ ...form, email: e.target.value })
        } else {
            return true
        }
    }
}