import express from "express";
import bodyParser from "body-parser";
import { set, connect } from "mongoose";
import Routes from './routes/index.js';
var app = express();
import cors from "cors";
import dotenv from "dotenv";
dotenv.config();


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var corsOptions = {
   origin: function (Origin, callback) {
      callback(null, true);
   },
   credentials: true,
};
app.use(cors(corsOptions));
// set("debug", true);
set("strictQuery", false);
const connectDB = async () => {
   const conn = await connect(`${process.env.MONGO_URI}`, {
      useUnifiedTopology: true,
      useNewUrlParser: true,
      autoIndex: true
   });
   console.log(`MongoDB Connected: ${conn.connection.host}`)
}
connectDB();
app.use('/', Routes);
app.get("/", (req, res) => {
   res.send("API is running....");
});

app.listen(process.env.PORT, () => {
   console.log("Server running on port 3001");
})
console.log("This is pid " + process.pid);

