import { Schema, model } from "mongoose";
import bcrypt from "bcryptjs";

const schema = new Schema(
  {
    firstName: { type: String, required: true },
    lastName: { type: String, required: true },
    dob: { type: String, required: true},
    email: { type: String, required: true, unique:true},
    password: { type: String, required: true },
    phone: { type: Number, required: true },
    status: {
      type: String,
      enum: ['PENDING','ACTIVE','DE-ACTIVE'],
      default: 'PENDING'
   }
  },
  {
    timestamps: true,
    toJSON: { virtuals: true },
    toObject: { virtuals: true },
  }
);

schema.pre("save", async function (next) {
  if (!this.isModified("password")) {
    next();
  }
  const salt = await bcrypt.genSalt(10);
  this.password = await bcrypt.hash(this.password, salt);
});

export const UserModel = model("User", schema);
