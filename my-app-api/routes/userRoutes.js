import express from "express"
import controller from '../controller/users.js';
const usersRouter = express.Router();

//Create a Single  user

usersRouter.post('/signup', controller.createUser);
usersRouter.get('/', controller.getAllUser);
usersRouter.get('/:id', controller.getSingleUser);
usersRouter.put('/:id', controller.updateSingleUser);
usersRouter.delete('/:id', controller.deleteSingleUser);
usersRouter.post('/login', controller.userLogin);
usersRouter.post('/change-password', controller.userChangePassword)
export default usersRouter;
