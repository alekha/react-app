import express from "express"
import usersRouter from "./userRoutes.js";

const route = express.Router();
route.use('/user', usersRouter);

export default route;