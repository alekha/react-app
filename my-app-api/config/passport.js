import { Strategy, ExtractJwt } from 'passport-jwt';
import jwt from "jsonwebtoken";
import Passport from 'passport';
import { UserModel } from "../model/users.js"
function passport(passport) {
  passport.use(
    new Strategy(
      {
        secretOrKey: process.env.SECRET_KEY,
        jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      },
      function (jwt_payload, CallBack) {
        UserModel.findOne({ email: jwt_payload.email }, function (err, user) {
          if (err) {
            return CallBack(err);
          }
          if (user) {
            CallBack(null, user);
          } else {
            CallBack(null, false);
          }
        });
      }
    )
  );
};

const authenticate = async (req, res, next) => {
  try {
    let authorization = req.header('authorization');
    let user = jwt.verify(authorization, process.env.SECRET_KEY)
    if (user.userId) {
      next();
    } else {
      res.status(200).send({ status: true, msg: `unauthorized}` });
    }
  }
  catch (error) {
    res.status(400).send({ status: false, error: `${error}` });
  }
}
export { passport, authenticate };