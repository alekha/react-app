import asyncHandler from "express-async-handler";
import cloudinary from "cloudinary";
const uploadAsset = asyncHandler(async (req, res) => {

    // cloudinary config detail I can not share here
    
  cloudinary.v2.config({
    cloud_name: process.env.CLOUDINARY_CLOUD_NAME,
    api_key: process.env.CLOUDINARY_KEY,
    api_secret: process.env.CLOUDINARY_SECRET,
  });
  let uploadeResponse = await cloudinary.v2.uploader.upload(req.data)
  if (uploadeResponse.overwritten) {
    uploadeResponse = await cloudinary.v2.uploader.upload(req.data, { public_id: req.name + '_' + Math.random().toFixed(2) * 100 })
  }

  return uploadeResponse.secure_url;
})
export default uploadAsset;