import { UserModel } from "../model/users.js";
import bcrypt from "bcryptjs";
import jwt from "jsonwebtoken";


// create user
const createUser = async (req, res) => {
    try {
        const user = new UserModel(req.body);
        const result = await user.save();
        res.status(201).send({ status: true, msg: 'User registerd successfully', data: result });
    } catch (error) {
        res.status(400).send({ status: false, error: error, msg: error?.keyPattern?.email ? 'User is already registered' : '' });
    }
}

// user Login
const userLogin = async (req, res) => {
    try {
        const { email, password } = req.body;

        const users = await UserModel.findOne({ email: email });
        if (!users) {
            return res.status(200).json({ status: 0, msg: "User Not Found!" });
        }
        const user = users;
        bcrypt.compare(password, user.password).then((isMatch) => {
            if (isMatch) {
                const payload = {
                    userId: user._id,
                    firstName: user.firstName,
                    email: user.email,
                };
                jwt.sign(
                    payload,
                    process.env.SECRET_KEY,
                    { expiresIn: 36000 },
                    (err, token) => {
                        if (!err) {
                            res.json({
                                status: 1,
                                msg: "Login Successfully",
                                token: token,
                                user_id: user._id,
                                user: {
                                    userId: user._id,
                                    email: user.email,
                                    firstName: user.firstName
                                },
                            });
                        } else {
                            res.json({ msg: "Error generating token" });
                        }
                    }
                );
            } else {
                res.status(200).json({ status: 0, msg: "password Incorrect!" });
            }
        });
    } catch (error) {
        res.status(400).send({ status: false, msg: "something went wrong" });
    }
}

// get single user detail by passing its id in params
export const getSingleUser = async (req, res) => {
    try {
        const { id } = req.params;
        const user = await UserModel.findById({ _id: id })
        if (user) {
            res.status(200).send({ status: true, msg: "User detail retrived", user: user })
        } else {
            res.status(200).json({ status: 0, msg: "User Not Found" });
        }
    } catch (error) {
        res.status(400).send({ status: false, msg: "something went wrong" });
    }
};

// delete single user detail by passing its id in params
export const deleteSingleUser = async (req, res) => {
    try {
        const { id } = req.params;
        UserModel.findByIdAndDelete(id, (err, deletedUser) => {
            if (err) {
                res.status(200).json({ status: 0, msg: "Error deleting user", err: err });

            } else {
                res.status(200).send({ status: true, msg: "User Deleted", user: deletedUser })
            }
        });
    } catch (error) {
        res.status(400).send({ status: false, msg: "something went wrong" });
    }
};

// update single user detail by passing its id in url
export const updateSingleUser = async (req, res) => {
    try {
        const { id } = req.params;
        const { firstName, lastName, dob, email, phone ,status} = req.body;
        const data = {
            firstName: firstName,
            lastName: lastName,
            dob: dob,
            email: email,
            phone: phone,
            status:status
        };
        UserModel.findByIdAndUpdate(id, data, { new: true }, (err, updatedUser) => {
            if (err) {
                res.status(200).json({ status: 0, msg: "Error deleting user", err: err });
            } else {
                res.status(200).send({ status: true, msg: "User Updated", user: updatedUser })
            }
        }
        );
    } catch (error) {
        res.status(400).send({ status: false, msg: "something went wrong" });
    }
};

// fetching users detail
export const getAllUser = async (req, res) => {
    try {
        const count = await UserModel.countDocuments();
        const users = await UserModel.find();
        return res.status(200).send({ status: true, msg: 'Users fetch successfully', count, users });
    } catch (error) {
        res.status(400).send({ status: false, msg: "something went wrong" });
    }
};

export const userChangePassword = async (req, res) => {
    try {
        const { email, password, newPassword } = req.body;
        const user = await UserModel.findOne({ email: email });
        if (user) {
            // Check if the current password matches
            const passwordMatches = await bcrypt.compare(password, user.password);
            if (!passwordMatches) {
                return res.status(401).json({ msg: 'Incorrect current password' });
            }
            // Update the user's password in the database
            user.password = newPassword;
            await user.save();
            res.status(200).send({status:true, msg: 'Password changed successfully' });

        } else {
            res.status(200).send({ status: true, msg: 'User not found' });

        }
    } catch (err) {
        res.status(400).send({ status: false, msg: "something went wrong" });
    }

}
export default {
    createUser,
    userLogin,
    deleteSingleUser,
    getSingleUser,
    updateSingleUser,
    getAllUser,
    userChangePassword
};